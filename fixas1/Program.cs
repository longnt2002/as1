﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace fixas1
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<GiaoVien> gv = new List<GiaoVien>();
            string HoTen1;
            int NamSinh1;
            double LuongCoBan1;
            double HeSoLuong1;
            int sgv;

            Console.Write("So giao vien muon nhap: ");
            while (!int.TryParse(Console.ReadLine(), out sgv) || sgv <= 0)
            {
                Console.Write("Nhap lai so giao vien muon nhap (la 1 so nguyen lon hon 0): ");
            }
            Console.WriteLine("------------------------------------");
            for (int i = 0; i < sgv; i++)
            {
                Console.Write("Ho ten: ");
                HoTen1 = Console.ReadLine();
                Console.Write("Nam sinh: ");
                while (!int.TryParse(Console.ReadLine(), out NamSinh1) || NamSinh1 <= 0 || NamSinh1 > 2023)
                {
                    Console.Write("Nhap lai nam (la 1 so nguyen lon hon 0 va be hon 2023): ");
                }
                Console.Write("Luong co ban: ");
                while (!double.TryParse(Console.ReadLine(), out LuongCoBan1) || LuongCoBan1 < 0)
                {
                    Console.Write("Nhap lai luong co ban (la 1 so nguyen khong am): ");
                }
                Console.Write("He so luong: ");
                while (!double.TryParse(Console.ReadLine(), out HeSoLuong1) || HeSoLuong1 <= 0)
                {
                    Console.Write("Nhap lai he so luong (la 1 so nguyen lon hon 0): ");
                }
                Console.WriteLine("------------------------------------");
                GiaoVien g = new GiaoVien(HoTen1, NamSinh1, LuongCoBan1, HeSoLuong1);
                gv.Add(g);

            }
            Console.WriteLine("------------------------------------");
            foreach (var item in gv)
            {
                item.XuatThongTin1();
            }
            Console.WriteLine("------------------------------------");
            GiaoVien GiaoVienLuongThapNhat = gvluongmin(gv);
            GiaoVienLuongThapNhat.XuatThongTin1();
            Console.Read();
        }
        static GiaoVien gvluongmin(List<GiaoVien> gv)
        {
            GiaoVien gvmin = gv[0];
            double luongmin = gvmin.TinhLuong1();

            foreach (var giaoVien in gv)
            {
                double luong = giaoVien.TinhLuong1();
                if (luong < luongmin)
                {
                    luongmin = luong;
                    gvmin = giaoVien;
                }
            }

            return gvmin;
        }
    }
}
