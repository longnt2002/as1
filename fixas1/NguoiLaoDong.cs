﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fixas1
{
    internal class NguoiLaoDong
    {
        string HoTen;
        int NamSinh;
        double LuongCoBan;
        public string HoTen1 { get => HoTen; set => HoTen = value; }
        public int NamSinh1 { get => NamSinh; set => NamSinh = value; }
        public double LuongCoBan1 { get => LuongCoBan; set => LuongCoBan = value; }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }
        public NguoiLaoDong()
        {

        }
        public void NhapThongTin(string HoTen1, int NamSinh1, double LuongCoBan1)
        {
            HoTen = HoTen1;
            NamSinh = NamSinh1;
            LuongCoBan = LuongCoBan1;
        }
        public double TinhLuong()
        {
            return LuongCoBan;
        }
        public void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
        }
    }
}
