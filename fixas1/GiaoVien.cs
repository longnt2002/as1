﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fixas1
{
    internal class GiaoVien : NguoiLaoDong
    {
        double HeSoLuong;
        public double HeSoLuong1 { get => HeSoLuong; set => HeSoLuong = value; }
        public GiaoVien()
        {

        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }
        public void NhapThongTin(double HeSoLuong1)
        {
            HeSoLuong = HeSoLuong1;
        }
        public double TinhLuong1()
        {
            return TinhLuong() * HeSoLuong1 * 1.25;
        }
        public void XuatThongTin1()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong: {HeSoLuong1}, Luong: {TinhLuong1()}");
        }
        public double XuLy()
        {
            return HeSoLuong + 0.6;
        }      
    }
}
